package AS3_Crypto.Hurlant;


import java.math.BigInteger;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WORKGROUP
 */
public class DiffieHellman {
    private Integer bLenght = 32;
    public BigInteger Prime;
    public BigInteger Generator;
    private BigInteger PrivateKey;
    public BigInteger PublicKey;
    
    public DiffieHellman(int i) {
        bLenght = i;
        Init(true);
    }
    public DiffieHellman(String prime, String generator, int i) {
        bLenght = i;
        Prime = new BigInteger(prime);
        Generator = new BigInteger(generator);
        Init(false);
    }
    private void Init(boolean generate) {
        this.PublicKey = BigInteger.ZERO;
        Random rand = new Random();
        while (this.PublicKey == BigInteger.ZERO) {
            if (generate) {
                this.Prime = BigInteger.probablePrime(bLenght, rand);
                this.Generator = BigInteger.probablePrime(bLenght, rand);
            }
            byte[] bytes = new byte[bLenght / 8];
            this.PrivateKey = new BigInteger(bytes);
            if (this.Generator.intValue() > this.Prime.intValue()) {
                BigInteger temp = this.Prime;
                this.Prime = this.Generator;
                this.Generator = temp;
            }
            this.PublicKey = this.Generator.modPow(this.PrivateKey, this.Prime);
        }
    }
    public BigInteger CalculateSharedKey(BigInteger m) {
         return m.modPow(this.PrivateKey, this.Prime);
    }
}
