package AS3_Crypto.Hurlant;


import java.math.BigInteger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WORKGROUP
 */
public class RSA {
    // public key
    public BigInteger e; // public exponent. must be <2^31
    public BigInteger n; // modulus
    // private key
    public BigInteger d;
    // extended private key
    public BigInteger p;
    public BigInteger q;
    public BigInteger dmp1;
    public BigInteger dmq1;
    public BigInteger coeff;
    protected boolean canDecrypt;
    protected boolean canEncrypt;
    
    public RSA(BigInteger _n, BigInteger _e, BigInteger _d, BigInteger _p, BigInteger _q, BigInteger _dp, BigInteger _dq, BigInteger _c) {
        this.n = _n;
        this.e = _e;
        this.d = _d;
        this.p = _p;
        this.q = _q;
        this.dmp1 = _dp;
        this.dmq1 = _dq;
        this.coeff = _c;
        // adjust a few flags.
        canEncrypt = (n != BigInteger.ZERO && e != BigInteger.ZERO);
        canDecrypt = (canEncrypt && d != BigInteger.ZERO);
    }
    public static RSA parsePublicKey(String N, Long E) {
        return new RSA(new BigInteger(N, 16), BigInteger.valueOf(E), BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO);
    }
    public static RSA parsePrivateKey(String _n, Long _e, String _d, String _p, String _q, String _dp, String _dq, String _c) {
        if(_p == null || _p == "") {
            return new RSA(new BigInteger(_n, 16), BigInteger.valueOf(_e), new BigInteger(_d, 16), BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO);
        }
        else {
            return new RSA(new BigInteger(_n, 16), BigInteger.valueOf(_e), new BigInteger(_d, 16), new BigInteger(_p, 16), new BigInteger(_q, 16), new BigInteger(_dp, 16), new BigInteger(_dq, 16), new BigInteger(_c));
        }
    }
    public int getBlockSize() { return (n.bitLength() + 7) / 8; }
    
    /**
     * PKCS#1 pad. type 2, random.
     * puts as much data from src into it, leaves what doesn't fit alone.
     */
    private byte[] pkcs1pad(byte[] src, Integer n) {
        byte[] out = new byte[n];
        Integer p = 0;
        Integer i = src.length - 1;
        while(i >= p && n >= 11) {
            out[--n] = src[i--];
        }
        out[--n] = 0;
        while(n > 2) {
            int x = 0xFF;
            out[--n] = (byte)x;
        }
        out[--n] = (byte)2;
        out[--n] = 0;
        return out;
    }
    private byte[] pkcs1unpad(BigInteger src, Integer n) {
        byte[] b = src.toByteArray();
        int i = 0;
        while (i < b.length && b[i] == 0) ++i;
        if (b.length-i != n-1 || b[i]!=2) { return null; }
        ++i;
        while (b[i] != 0) {
            if (++i >= b.length) { return null; }
        }
        byte[] out = new byte[b.length - i + 1];
        for(int q = 0; ++i < b.length; ) {
            out[q] = b[i];
            q++;
        }
        return out;
    }
    
    /**
     * @return a new random private key B bits long, using public expt E
     */
    protected BigInteger doPublic(BigInteger x) { return x.modPow(e, n); }
    protected BigInteger doPrivate(BigInteger x) { return x.modPow(d, n); }
    protected BigInteger doPrivate2(BigInteger x) {
        if (p == BigInteger.ZERO && q == BigInteger.ZERO) return x.modPow(d, n);
        // TODO: re-calculate any missing CRT params
        BigInteger xp = x.mod(p).modPow(dmp1, p);
        BigInteger xq = x.mod(q).modPow(dmq1, q);
        while (xp.compareTo(xq) < 0) {
            xp = xp.add(p);
        }
        return xp.subtract(xq).multiply(coeff).mod(p).multiply(q).add(xq);
    }
    
    /**
     * Encrypt / Decrypt
     */
    public byte[] encrypt(byte[] src, String Q) {
        Integer bl = this.getBlockSize();
        BigInteger block = new BigInteger(this.pkcs1pad(src, bl));
        BigInteger chunk;
        switch(Q) {
            case "private":
                chunk = this.doPrivate2(block);
                break;
            default:
                chunk = this.doPublic(block);
                break;
        }
        return chunk.toByteArray();
                
    }
    public byte[] decrypt(byte[] src, String Q) {
        Integer bl = this.getBlockSize();
        BigInteger block = new BigInteger(new String(src), 16);
        BigInteger chunk;
        switch(Q) {
            case "public":
                chunk = this.doPublic(block);
                break;
            default:
                chunk = this.doPrivate2(block);
                break;
        }
        return this.pkcs1unpad(chunk, bl);
    }
}
