package AS3_Crypto.Hurlant;


import java.math.BigInteger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WORKGROUP
 */
public class RsaKeys {
    private BigInteger _n;
    private BigInteger _e;
    private BigInteger _d;
    
    public RsaKeys(String n, int e, String d) {
        _n = new BigInteger(n, 16);
        _e = BigInteger.valueOf(e);
        _d = new BigInteger(d, 16);
    }
    
    public BigInteger getN() { return _n; }
    public BigInteger getE() { return _e; }
    public BigInteger getD() { return _d; }
}
