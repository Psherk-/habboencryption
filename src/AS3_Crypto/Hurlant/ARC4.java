/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AS3_Crypto.Hurlant;

/**
 *
 * @author WORKGROUP
 */
public class ARC4 {
    private int i = 0;
    private int j = 0;
    private byte[] S;
    private byte[] Key;
    private int psize = 256;
        
    public ARC4(byte[] key) {
        S = key;
        if(key != null) {
            init(key);
        }
    }
    public int getPoolSize() { return psize; }
    private void init(byte[] key) {
        this.Key = key; //keep a copy, as we need to reset our state for every encrypt/decrypt call.
        int i;
        int j;
        int t;
        for (i = 0; i<256; ++i) {
            S[i] = (byte) i;
        }
        j = 0;
        for (i=0; i<256; ++i) {
            j = (j + S[i] + key[i%key.length]) & 255;
            t = S[i];
            S[i] = S[j];
            S[j] = (byte) t;
        }
        this.i=0;
        this.j=0;
    }
    public int next() {
        int t;
        i = (i+1)&255;
        j = (j+S[i])&255;
        t = S[i];
        S[i] = S[j];
        S[j] = (byte) t;
        return S[(t+S[i])&255];
    }
    public int getBlockSize() { return 1; }
    public void encrypt(byte[] block) {
        init(Key);
        int i = 0;
        while (i<block.length) {
            block[i++] ^= next();
        }
    }
    public void decrypt(byte[] block) {
        encrypt(block); // the beauty of XOR.
    }
    public void dispose() {
         int i = 0;
         for (i=0;i<S.length;i++) {
             S[i] = (byte) (Math.random()*256);
         }
         for (i=0;i<Key.length;i++) {
             Key[i] = (byte) (Math.random()*256);
         }
         S = null;
         Key = null;
         this.i = 0;
         this.j = 0;
    }
}
