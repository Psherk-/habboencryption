/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AS3_Crypto.Convert;

import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author WORKGROUP
 */
public class HexBinary {
    public static byte[] HexBinaryToBytes(String arg) {
        int x = arg.length();
        byte[] bytes = new byte[x / 2];
        for (int i = 0; i < x; i += 2) {
            bytes[i / 2] = Byte.parseByte(arg.substring(i, 2), 16);
        }
        return bytes;
    }
    public static String BytesHexBinary(byte[] arg) {
        String hexstring = new String(arg);
        return hexstring.replace("-", "");
    }
}
